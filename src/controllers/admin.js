const jwtHelper = require('../helpers/jwt')
const adminService = require('../services/admin');

const accessTokenLife = "20m";

const accessTokenSecret = "ahshfuhfusdughusdgsdhs";

module.exports.loginAdmin = async function(req, res) {
    try {
        let email = req.body.email;
        let password = req.body.password;
        const user = await adminService.checkAccount(email, password);
        
        if (user) {
            let userData = {};
            userData.email = email;
            const accessToken = await jwtHelper.generateToken(userData, accessTokenSecret, accessTokenLife);
            return res.status(200).json({accessToken});
        } else {
            return res.status(404).json("Tai khoan hoac mat khau khong dung");
        }
              
      } catch (error) {
        return res.status(500).json(error);
      }

}

