const bookService = require('../services/book');
const { v4: uuidv4 } = require('uuid');

module.exports.getAllBook = async function(req, res) {
    try {
        const books = await bookService.getAllBook();
        res.send(books);
    } catch {
        res.status(500).send('fail');
    }
}

module.exports.getBookById = async function(req, res) {
    try {
        let id = req.params.id;
        const bookById = await bookService.getAllBookById(id);  
        res.send(bookById);
    } catch(error) {
        res.status(500).send('fail');
    }
}

module.exports.getBooksByIdUser = async function(req, res) {
    try {
        let idUser = req.params.id;
        const booksByIdUser = await bookService.getAllBookById(idUser);  
        res.send(booksByIdUser);
    } catch(error) {
        res.status(500).send('fail');
    }
}

module.exports.createNewBook = async function(req, res) {
    try {
        let data = {};
        data.uuid = uuidv4();
        data.name = req.body.name;
        data.idUser = req.body.idUser;
        const book = await bookService.createNewBook(data);
        res.status(200).send('success ');
    } catch(error) {    
        res.status(500).send(error);
    }
}

module.exports.deleteBook = async function(req, res) {
    try {
        let id = req.params.id;
        const deleteBook = await bookService.deleteBook(id);
        res.status(200).send('success');
    } catch(error) {
        res.status(500).send(error)
    }
}