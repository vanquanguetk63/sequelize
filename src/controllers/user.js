const userService = require('../services/user');
const { v4: uuidv4 } = require('uuid');

module.exports.getAllUser = async function(req, res) {
    try {
        const users = await userService.getAllUser();
        res.send(users);
    } catch {
        res.status(500).send(error);
    }
}

module.exports.getUserById = async function(req, res) {
    try {
        let id = req.params.id;
        const users = await userService.getUserById(id);  
        res.send(users);
    } catch(error) {
        res.status(500).send(error);
    }
}

module.exports.getBooksByIdUser = async function(req, res) {
    try {
        let idUser = req.params.id;
        const books = await userService.getBooksByIdUser(idUser);  
        res.send(books);
    } catch(error) {
        res.status(500).send(error);
    }
}

module.exports.createNewUser = async function(req, res) {
    try {
        let data = {};
        data.uuid = uuidv4();
        data.email = req.body.email;
        data.phone = req.body.phone;
        const users = await userService.createNewUser(data);
        res.status(200).send('success: ');
    } catch(error) {    
        res.status(500).send(error);
    }
}

module.exports.deleteUserById = async function(req, res) {
    try {
        let id = req.body.id;
        const users = await userService.deleteUserById(id);
        if (users) {
            res.status(200).send('success: ');
        }
    } catch(error) {    
        res.status(500).send(error);
    }
}