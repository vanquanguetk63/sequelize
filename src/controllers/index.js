const bookController = require('./book');
const userController = require('./user');
const adminController = require('./admin');


module.exports = {
    bookController,
    userController,
    adminController
}