const db = require('../../models/index');
const bookModels = db['books'];
const userModels = db['users'];

const getAllUser = async () => {
    let users = await userModels.findAll();
    return users;
}

const getUserById = async (id) => {
    let user = await userModels.findOne({
        where: {
            id: id
        }
    });
    return user;
}

const getBooksByIdUser = async (idUser) => {
    console.log(idUser)
    let books = await bookModels.findAll({
        include: [{
            model: userModels,
            where: {
                id: idUser
            }
        }],
        
    });
    return books;
}

const createNewUser = async (data) => {
    try {
        let user = await userModels.create({
            id: data.uuid,
            email: data.email,
            phone: data.phone,
        });
        return true;
    } catch {
        return false;
    }
}

const deleteUserById = async (id) => {
    try {
        let user = await userModels.destroy({
            where: {
                id: id
            }
        });
        return true;
    } catch {
        return false;
    }
}

module.exports = {
    getAllUser,
    getUserById,
    getBooksByIdUser,
    createNewUser,
    deleteUserById,
}