const db = require('../../models/index');
const bookModels = db['books'];
const userModels = db['users'];

const getAllBook = async () => {
    let books = await bookModels.findAll();
    return books;
}

const getBookById = async (id) => {
    let books = await bookModels.findAll({
        where: {
            id: id
        }
    });
    return books;
}

const getBooksByIdUser = async (idUser) => {
    let books = await bookModels.findAll({
        include: [{
            model: userModels,
            attributes: ['username', 'phone']
        }],
        where: {
            idUser: idUser
        }
    })
    return books
}

const createNewBook = async (data) => {
    try {
        let book = await bookModels.create({
            id: data.uuid,
            name: data.name,
            idUser: data.idUser,
        });
        return true;
    } catch(error) {
        console.log(error);
        return false;
    }
}

const deleteBook = async (id) => {
    try {
        let book = await bookModels.destroy({
            where: {
                id: id
            }
        });
        return true;
    } catch {
        return false;
    }
}

module.exports = {
    getAllBook,
    getBookById,
    getBooksByIdUser,
    createNewBook,
    deleteBook
}