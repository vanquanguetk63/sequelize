const db = require('../../models/index');
const adminModels = db['admins'];

const checkAccount = async (email, password) => {
    try {
        const admin = await adminModels.findOne({
            where: {
                email: email,
                password: password
            }
        })
        if (admin) {
            return true;
        } else {
            return false;
        }
    } catch {
        return false;
    }
}

module.exports = {
    checkAccount
}