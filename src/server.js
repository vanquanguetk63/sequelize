const express = require("express")
const app = express();
const port = 3003;
const bodyParser = require('body-parser');

const router = require('./routes/index');

app.use(bodyParser.json());

app.use(router);

app.listen(port, () => {
  console.log("SV is opened");
});
