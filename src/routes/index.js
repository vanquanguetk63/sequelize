const router = require('express').Router();
const bookRoute = require('./book');
const userRoute = require('./user');
const adminRoute = require('./admin');
const auth = require('../middlewares/auth');

router.use(adminRoute);
router.use(auth.isAuth);
router.use(bookRoute);
router.use(userRoute);

module.exports = router;