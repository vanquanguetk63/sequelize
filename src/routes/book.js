const controller = require('../controllers/index');
const router = require('express').Router();

router.get('/api/v1/book/', controller.bookController.getAllBook);

router.get('/api/v1/book/:id', controller.bookController.getBookById);

router.post('/api/v1/book/create', controller.bookController.createNewBook);

module.exports = router;