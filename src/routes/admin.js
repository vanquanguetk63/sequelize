const controller = require('../controllers/index');
const router = require('express').Router();

router.post('/api/v1/admin/login', controller.adminController.loginAdmin);

module.exports = router;