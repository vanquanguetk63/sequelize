const controller = require('../controllers/index');
const router = require('express').Router();

router.get('/api/v1/user/', controller.userController.getAllUser);

router.get('/api/v1/user/:id', controller.userController.getUserById);

router.get('/api/v1/user/:id/books', controller.userController.getBooksByIdUser);

router.post('/api/v1/user/create', controller.userController.createNewUser);

router.delete('/api/v1/user/:id', controller.userController.deleteUserById);

module.exports = router;