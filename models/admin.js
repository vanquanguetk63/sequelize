module.exports = (sequelize, DataTypes) => {
  const Admins = sequelize.define(
    "admins",
    {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: DataTypes.UUID,
      },
      email: {
        type: DataTypes.STRING,
      },
      password: {
        type: DataTypes.STRING
      }
    },
    {}
  );
  Admins.associate = function (models) {
  };

  return Admins;
};
