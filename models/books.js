module.exports = (sequelize, DataTypes) => {
  const Books = sequelize.define(
    "books",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.UUID,
      },
      name: {
        type: DataTypes.STRING,
      },
      idUser: {
        type: DataTypes.UUID,
      },
    },
    {}
  );
  Books.associate = function (models) {
    Books.belongsTo(models.users, {
      foreignKey: 'idUser'
    })
  };

  return Books;
};
